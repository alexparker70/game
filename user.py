import do
import random

#TODO: Add comments, write more code

def new_player(atk,dfc,strength,end,intel,dex,frt,name,specie,type):
    stats = [atk,dfc,strength,end,intel,dex,frt]
    nom = ['atk','dfc','strength','end','intel','dex','frt']
    i = 0
    lvl = 0
    exp = 0
    exp_to_lvlup = 100
    name = name
    with open(name + '_stats.csv', 'a+') as w:
        w.write('Player,'+name+'\nspecie,'+specie+'\nType,'+type+'\nLevel,'+str(lvl)+'\nexp,'+str(exp)+'\nexp to lvl up,'+str(exp_to_lvlup)+'\n')
    for x in stats:

        roll = do.dice_roll(6)
        mult = roll-2
        mult = roll*1.25
        x = x*mult
        #print mult

        #print x
        #print nom[i]
        x=float("{0:.2f}".format(x))
        with open(name+'_stats.csv','a+') as w:
            w.write(nom[i]+','+str(x)+'\n')
        i += 1

    #maybe this should go elsewhere...
    '''print atk
    print strength
    atkstr = strength/10
    atkstr = str(atkstr)
    atkstr = float("1"+atkstr)-9
    print atkstr
    atk = atk * atkstr
    print atk'''

def levelup(player):
    with open('typemult.csv')as f , open(player+'_stats.csv') as f1:
        lines1 = f1.readlines()

        s = []
        t = []
        for line in f:
            #print line
            line = line.split(',')
            #print line
            #print lines1[1]
            str1 = line[0]
            str2 = lines1[1].split(',')
            str2 = str2[1]
            str2 = str2.replace('\n','')
            str1,str2 = str1.lower(),str2.lower()

            if str1 == str2:
                for x in line:
                    s.append(x)

            if (line[0]) == str(lines1[2].split(',')[1].replace('\n','')):
                for x in line:
                    t.append(x)
        typemult = []
        for x,i in zip(s[1:],t[1:]):
            x,i=float(x),float(i)
            z = x+i
            typemult.append(z)

    #print typemult
    with open(player+'_stats.csv') as f:
        nom = ['atk','dfc','strength','end','intel','dex','frt']
        i=0
        string = []
        m = 0
        z=0
        #print typemult
        for line in f:
            x=line.split(',')
            #print x

            if 'Player' in x[0]:
                string.append(str(x[0])+','+str(x[1])+'\n')
            elif 'exp'in x[0]:
                string.append(str(x[0]) +','+ str(x[1])+'\n')
            elif 'specie'in x[0]:
                string.append(str(x[0]) +','+ str(x[1]) + '\n')
            elif 'Type'in x[0]:
                string.append(str(x[0]) +','+ str(x[1]) + '\n')
            elif x[0] == 'Level':
                level = int(x[1])
                level += 1
                string.append(str(x[0]) +','+ str(level)+'\n')

            elif x[0] == 'exp to lvl up':
                lvlup=int(x[1])
                lvlup *= 2.1
                string.append(str(x[0]) +','+ str(lvlup)+'\n')
            else:

                roll = do.dice_roll(3)
                z+=roll
                mult = z * 1.25
                y = float(x[1])
                y *= mult
                y *= float(typemult[m])
                #print typemult[m]
                #print x[0]
                m+=1
                string.append(str(x[0]) +','+ str(y)+'\n')

    with open(player+'_stats.csv','w') as f:
        for x in string:
            x=x.replace('\n','')
            f.write(x+'\n')



        #level = lines[1].split(',')[1]
        #level += 1
        #exp_to_lvlup = lines[3]

def c_user(player,IFF,**kwargs):
    #create computer friend/foes/neutrals with similar level as player

    #get user level

    if kwargs is not None:
        d = kwargs
        globals().update(d)

    with open(player+'_stats.csv') as f:
        level_line = f.readlines()[3]
        level = level_line.split(',')[1]

    def foe():
        atk, dfc, strength, end, intel, dex, frt, specie = 2, 2, 2, 2, 2, 2, 2, 'human'
        foo = ['fighter','rogue','wizard','bowman','gunslnger']
        type = random.choice(foo)
        name ='tmp_foe'
        with open (name+'_stats.csv','a+') as w:
            w.write('Player,' + name + '\nspecie,' + specie + '\nType,' + type + '\nLevel,' + str(0) + '\nexp,' + str(0) + '\nexp to lvl up,' + str(0) + '\n')

        stats = [atk, dfc, strength, end, intel, dex, frt]
        nom = ['atk', 'dfc', 'strength', 'end', 'intel', 'dex', 'frt']
        i=0
        for x in stats:
            roll = do.dice_roll(6)
            mult = roll - 2
            mult = roll * 1.25
            x = x * mult
            x = float("{0:.2f}".format(x))
            with open(name + '_stats.csv', 'a+') as w:
                w.write(nom[i] + ',' + str(x) + '\n')
            i += 1

        #print atk, dfc, strength, end, intel, dex, frt, specie, type
        for i in range(0,int(level),1):
            levelup('tmp_foe')

    def kill_foe():
        with open('tmp_foe_stats.csv','w') as w:
             w.write('')

    def friend(name):
        atk, dfc, strength, end, intel, dex, frt, specie = 2, 2, 2, 2, 2, 2, 2, 'human'
        foo = ['fighter','rogue','wizard','bowman','gunslnger']
        type = random.choice(foo)

        with open (name+'_stats.csv','a+') as w:
            w.write('Player,' + name + '\nspecie,' + specie + '\nType,' + type + '\nLevel,' + str(0) + '\nexp,' + str(0) + '\nexp to lvl up,' + str(0) + '\n')

        stats = [atk, dfc, strength, end, intel, dex, frt]
        nom = ['atk', 'dfc', 'strength', 'end', 'intel', 'dex', 'frt']
        i=0
        for x in stats:
            roll = do.dice_roll(6)
            mult = roll - 2
            mult = roll * 1.25
            x = x * mult
            x = float("{0:.2f}".format(x))
            with open(name + '_stats.csv', 'a+') as w:
                w.write(nom[i] + ',' + str(x) + '\n')
            i += 1

        #print atk, dfc, strength, end, intel, dex, frt, specie, type
        for i in range(0,int(level),1):
            levelup(friend_name)

    if IFF == 'friend':
        friend(friend_name)
    elif IFF == 'foe':
        foe()
    else:
        pass

#new_player(2,2,2,2,2,2,2,'player1','human','fighter')
#levelup('player1')
#c_user('player1')